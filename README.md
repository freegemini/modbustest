# MODBUS - практика  

[![Build status](https://ci.appveyor.com/api/projects/status/hec1abyleavca97p?svg=true)](https://ci.appveyor.com/project/freegemini/modbustest) [![pipeline status](https://gitlab.com/freegemini/modbustest/badges/main/pipeline.svg)](https://gitlab.com/freegemini/modbustest/commits/main) [![coverage](https://gitlab.com/freegemini/modbustest/badges/main/coverage.svg)](https://gitlab.com/freegemini/modbustest/commits/main) [![codecov](https://codecov.io/gl/freegemini/modbustest/branch/\x6d61696e/graph/badge.svg?token=VZHS5SRCWT)](https://codecov.io/gl/freegemini/modbustest)  

Тестовые проекты для отработки навыков работы с этим протоколом.  

* Начал с [этой](https://web.flow.opera.com/ext/v1/index-f24f6cbdb366dc16a38b9c0050050743c876df97b714286d87fec8621b0785d1.html) статьи.  
* При отладке пользуюсь эмуляторами _ModRSsim2.exe_ и _pyModSlave-0.4.4-beta-Win32_  
* В проекте используется NuGet пакет [NModbus](https://www.nuget.org/packages/NModbus/)
