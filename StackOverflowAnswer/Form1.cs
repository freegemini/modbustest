﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using EasyModbus;
using System.Windows.Forms;


namespace StackOverflowAnswer
{
    public partial class Form1 : Form
    {
        ModbusClient modbusClient;

        public Form1()
        {
            InitializeComponent();
            ON.Visible = true; //yellow
            OFF.Visible = true;//red
            modbusClient = new ModbusClient("127.0.0.1", 502);//communication settings ### changed to TCP - i have no COM ports ###
            //modbusClient.UnitIdentifier = 1;
            //modbusClient.Baudrate = 19200;
            //modbusClient.Parity = System.IO.Ports.Parity.None;
            //modbusClient.StopBits = System.IO.Ports.StopBits.One;
            modbusClient.Connect();

            #region Modified section
            var readModBusTimer = new Timer()
            {
                Interval = 500,
                Enabled = true
            };

            readModBusTimer.Tick += ReadModBusTimer_Tick;
            readModBusTimer.Start();

            // reading function begin
            ReadCoils();

            // end of reading function

        }

        private void ReadCoils()
        {
            var value = modbusClient.ReadCoils(0, 1);// read coil zero and if it is true change color yellow else change color to red


            if (value[0] == true)
            {
                ON.Visible = true;
                OFF.Visible = false;
            }
            else
            {
                ON.Visible = false;
                OFF.Visible = true;
            }
        }

        private void ReadModBusTimer_Tick(object sender, EventArgs e)
        {
            ReadCoils();
        }
        #endregion
        private void button1_Click(object sender, EventArgs e)// on button
        {
            modbusClient.WriteSingleCoil(0, true);//toggle coil zero to on
        }

        private void button2_Click(object sender, EventArgs e)// off button
        {

            modbusClient.WriteSingleCoil(0, false);// toggle coil zero to off
        }


    }
}



