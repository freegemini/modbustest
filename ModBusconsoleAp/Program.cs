﻿using Colorful;

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;

using Console = Colorful.Console;

namespace ModBusconsoleAp
{
    public class Program
    {
        static int counter = 0;
        static bool exit = false;
        static StyleSheet styleSheet;

        
        public class CursorPos
        {
            public int Left;
            public int Top;            
        }

        static void Main(string[] args)
        {
            styleSheet = new StyleSheet(Color.FromArgb(200, 200, 200));
            styleSheet.AddStyle("result =", Color.FromArgb(196, 102, 227));
            styleSheet.AddStyle("counter", Color.Aquamarine);
            styleSheet.AddStyle("[Ee]rror", Color.Red);

            FigletFont font = FigletFont.Load(Path.Combine(Environment.CurrentDirectory, "FigletFonts\\banner3-D.flf"));
            Figlet figlet = new Figlet(font);
            Console.WriteLine(figlet.ToAscii("ModBus Test"), Color.FromArgb(196, 241, 200));

            //Console.Clear();
            //Console.WriteLine("Press 'F12' to exit. 'Spacebar' to start/stop sniffing.");
            Sniffer sniffer = null;
            ModBusRequest req = null;
            try
            {
                Console.SetCursorPosition(0, 8);
                Console.WriteLine("Press 'F12' to exit. 'Spacebar' to start/stop sniffing.\n");
                sniffer = new Sniffer("127.0.0.1", 502, 1, 1000);
                sniffer.DataReady += Sniffer_DataReady;

                req = new ModBusRequest() { DeviceAddress = 8, StartAddress = 0, NumRegisters = 10 };
                sniffer.Start(req);
            } catch(Exception ex)
            {
                Console.WriteLineStyled($"Some Error occur:\n {ex.Message}", styleSheet);
                exit = true;
            }

            #region Тест: создать два сниффера и повешать событие DataReady на один обработчик

            //var sniffer2 = new Sniffer("127.0.0.1", 502,2);

            ////sniffer2.DataReady += Sniffer_DataReady; работает и так
            //sniffer2.DataReady += Sniffer_DataReady2;       //  и так. Вывод друг на друга накладывается, но в принципе - работает

            //var req2 = new ModBusRequest() { DeviceAddress = 6, StartAddress = 0, NumRegisters = 5 };
            //sniffer2.Start(req2);
            #endregion

            while(!exit)
            {
                switch(Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        break;
                    case ConsoleKey.Spacebar:
                        if(sniffer.IsStarted)
                            sniffer.Stop();
                        else
                            sniffer.Start(req);
                        break;

                    case ConsoleKey.F12:
                        exit = true;
                        break;
                    default:
                        break;
                }
            }

            if(sniffer != null)
                sniffer.Stop();
        }

        private static void Sniffer_DataReady2(ModBusData e)
        {
            Console.SetCursorPosition(5, 2);
            Console.WriteLine($"Счетчик считывания данных: {++counter}");

            var result = e.HoldingRegisters;

            for(int i = 0; i < result.Length; i++)
            {
                Console.SetCursorPosition((e.SnifferId - 1) * 15, i + 10);
                ClearCurrentConsoleLine();
                Console.WriteLine($"result = {result[i]}");
            }
        }

        private static void Sniffer_DataReady(ModBusData e)
        {
            Console.CursorVisible = false;

            //Console.Clear();
            //Console.WriteLine("Press 'F12' to exit. 'Spacebar' to start/stop sniffing.");
            Console.SetCursorPosition(2, 10);
            Console.WriteLineStyled($"Read data counter: {++counter}", styleSheet);

            var result = e.HoldingRegisters;

            for(int i = 0; i < result.Length; i++)
            {
                ClearConsoleLineBlock(new CursorPos() { Left = 9, Top = i + 14 }, 5);
                Console.SetCursorPosition((e.SnifferId - 1) * 15, i + 14);

                // ClearCurrentConsoleLine();
                Console.WriteLineStyled($"result = {result[i]}", styleSheet);
            }
        }

        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        public static void ClearConsoleLineBlock(CursorPos curPos, int width)
        {
            CursorPos oldCursorPos = new CursorPos() { Left = Console.CursorLeft, Top = Console.CursorTop };
            Console.SetCursorPosition(curPos.Left, curPos.Top);
            Console.Write(new string(' ', width));
            Console.SetCursorPosition(oldCursorPos.Left, oldCursorPos.Top);
        }
    }
}
