﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModBusconsoleAp
{
    public class ModBusRequest:IDisposable
    {
        ushort startAddress;
        byte deviceAddress;
        ushort numRegisters;

        public ushort StartAddress { get => startAddress; set => startAddress = value; }
        public byte DeviceAddress { get => deviceAddress; set => deviceAddress = value; }
        public ushort NumRegisters { get => numRegisters; set => numRegisters = value; }

        public void Dispose()
        {
            startAddress=default;
            deviceAddress = default;
            numRegisters = default;
            GC.SuppressFinalize(this);
        }
    }
}
