﻿
using NModbus;

using System;
using System.Diagnostics;
using System.Net.Sockets;

namespace ModBusconsoleAp
{
    public class Sniffer
    {
        private int snifferId;
        private string hostname;
        private int port;
        private bool isStarted;
         private int checkInterval;
        System.Timers.Timer readingTimer;

        IModbusMaster master;
        ModBusRequest modBusRequest;

        /// <summary>
        /// IP-адрес modbus-slave
        /// </summary>
        public string Hostname { get => hostname; set => hostname = value; }
        /// <summary>
        /// Порт к которому требуется подключиться
        /// </summary>
        public int Port { get => port; set => port = value; }
        /// <summary>
        /// Интервал считывания значений в мс
        /// </summary>
        public int CheckInterval { get => checkInterval; set => checkInterval = value; }
        /// <summary>
        /// Определяет состояние считывателя
        /// </summary>
        public bool IsStarted { get => isStarted; set => isStarted = value; }
        /// <summary>
        /// ID считывателя. Сейчас устанавливается вручную при создании экземпляра объекта. В идеале: автоматически
        /// </summary>
        public int SnifferID { get => snifferId; set => snifferId = value; }
        /// <summary>
        /// Событие, срабатывающее при готовности считанных данных
        /// </summary>
        public event DataReadyEventHandler DataReady;
        public delegate void DataReadyEventHandler(ModBusData e);
        /// <summary>
        /// Создает экземпляр считывателя данных по шине ModBus с указанными параметрами
        /// </summary>
        /// <param name="hostname">IP-адрес modbus-slave</param>
        /// <param name="port">Порт к которому требуется подключиться</param>
        /// <param name="id">ID считывателя. Сейчас устанавливается вручную при создании экземпляра объекта. В идеале: автоматически</param>
        /// <param name="interval">Интервал считывания значений в мс</param>
        public Sniffer(string hostname, int port,int id, int interval = 500)
        {
            Hostname = hostname;
            Port = port;
            CheckInterval=interval;
            SnifferID = id;
            try
            {
                var client = new TcpClient(Hostname, Port);
                Debug.WriteLine($"Client ReceiveTimeout= {client.ReceiveTimeout}");
                Debug.WriteLine($"Client SendTimeout= {client.SendTimeout}");
                var factory = new ModbusFactory();
                master = factory.CreateMaster(client);
            }
            catch(Exception ex)
            {
                Debug.WriteLine($"Exception = {ex.Message}");
                Console.WriteLine($"Exception = {ex.Message}\n\n");
                throw (ex);
            }
            readingTimer=new System.Timers.Timer()
            {
                Interval=CheckInterval,
                AutoReset=true
            };
            readingTimer.Elapsed += ReadingTimer_Elapsed;
        }

        private void ReadingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Debug.WriteLine("Сработал таймер объекта Sniffer");
            //BUG: master.ReadHoldingRegisters не возвращает ничего. Вроде как таймаут 
            try
            {
                ushort[] tempa = master.ReadHoldingRegisters(modBusRequest.DeviceAddress, modBusRequest.StartAddress, modBusRequest.NumRegisters);
                var data = new ModBusData() { HoldingRegisters = tempa, SnifferId = this.SnifferID };
                if (data != null) DataReady?.Invoke(data);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Exception in ReadingTimer_Elapsed= {ex.Message}");
            }
        }
        /// <summary>
        /// Запускает процесс считывания данных с шины ModBus
        /// </summary>
        /// <param name="request">Параметры запроса <see cref="ModBusData"/></param>
        public void Start(ModBusRequest request)
        {
            modBusRequest = request;
            isStarted = true;
            readingTimer.Start();
        }
        public void Stop()
        {
            isStarted = false;
            readingTimer.Stop();
        }

        public ModBusData GetData(ModBusRequest request)
        {
            //var result = master.ReadHoldingRegisters(request.DeviceAddress, request.StartAddress, request.NumRegisters);
            //var a = result;
            return new ModBusData()
            {
                HoldingRegisters = master.ReadHoldingRegisters(request.DeviceAddress, request.StartAddress, request.NumRegisters)
        };
        }
    }
}
