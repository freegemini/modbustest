﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace ModBusconsoleAp
{   
    [TestFixture]
    public class ModbusUnitTests
    {
        static IEnumerable<object[]> Sniffers()
        {
            yield return new object[] { "127.0.0.1", 508, typeof(System.Net.Sockets.SocketException) };
            yield return new object[] { "10.10.0.0", 502, typeof(System.Net.Sockets.SocketException) };
            yield return new object[] { "127.0.0.1", 502, null };
        }


        [TestCaseSource(nameof(Sniffers))]
        public void TestConnectionExceptions(string addr,int port, Type exception = null)
        {
           // var req = new ModBusRequest() { DeviceAddress = 8, StartAddress = 0, NumRegisters = 10 };
            if (exception != null)
            {
                Assert.Throws(exception, delegate {new Sniffer(addr,port,1,500); });
            }

        }
    }
}
