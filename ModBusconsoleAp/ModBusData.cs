﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModBusconsoleAp
{
    public class ModBusData
    {
        ushort[] holdingRegisters;
        int snifferId;
        public ushort[] HoldingRegisters { get => holdingRegisters; set => holdingRegisters = value; }
        public int SnifferId { get => snifferId; set => snifferId = value; }
    }
}
